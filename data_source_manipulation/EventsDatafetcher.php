<?php
global $conn;
include 'db_connection.php';

$jsonData = file_get_contents('data_source_manipulation/datasource.json');
// FETCHING AND DECODING THE  DATA
$data = json_decode($jsonData, true);

//ITERATE OVER THE DATA AND CHECK WHETHER THE DATA EXIST IN THE DB OR NOT
foreach ($data as $item) {

    //GET THE EVENTS DATA
    $id = (int)$item['event_id'];
    $event_name = $conn->real_escape_string($item['event_name']);
    $event_date = $conn->real_escape_string($item['event_date']);


    $existingRecordQuery = "SELECT * FROM events WHERE id = $id";
    $existingRecordResult = $conn->query($existingRecordQuery);

    if ($existingRecordResult->num_rows == 0) {
        $insertQuery = "INSERT INTO events (id, event_name, event_date) VALUES ($id, '$event_name', '$event_date')";

        if ($conn->query($insertQuery) === false) {
            echo "Error: " . $insertQuery . "<br>" . $conn->error;
        } else {
            echo "Record with ID $id inserted successfully!<br>";
        }
    } else {
        echo "Record with ID $id already exists. Skipping.<br>";
    }
}

$conn->close();

echo "Data import process completed!";
?>