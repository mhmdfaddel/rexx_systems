<?php
global $conn;
include 'db_connection.php';

$jsonData = file_get_contents('data_source_manipulation/datasource.json');
// FETCHING AND DECODING THE  DATA
$data = json_decode($jsonData, true);

//ITERATE OVER THE DATA AND CHECK WHETHER THE DATA EXIST IN THE DB OR NOT
foreach ($data as $item) {

    //GET THE EMPLOYEES DATA
    $id = (int)$item['employee_id'];
    $employee_name = $conn->real_escape_string($item['employee_name']);
    $employee_mail = $conn->real_escape_string($item['employee_mail']);


    $existingRecordQuery = "SELECT * FROM employee WHERE id = $id";
    $existingRecordResult = $conn->query($existingRecordQuery);

    if ($existingRecordResult->num_rows == 0) {
        $insertQuery = "INSERT INTO employee (id, employee_name, employee_mail) VALUES ($id, '$employee_name', '$employee_mail')";

        if ($conn->query($insertQuery) === false) {
            echo "Error: " . $insertQuery . "<br>" . $conn->error;
        } else {
            echo "Record with ID $id inserted successfully!<br>";
        }
    } else {
        echo "Record with ID $id already exists. Skipping.<br>";
    }
}


$conn->close();

echo "Data import process completed!";
?>