<?php
// Database configuration
$servername = "localhost";
$username = "root";
$password = "";
$database = "rexx";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Set charset to utf8 (optional, depending on your requirements)
$conn->set_charset("utf8");
?>