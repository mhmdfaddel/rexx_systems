<?php
global $conn;
include 'db_connection.php';

$jsonData = file_get_contents('data_source_manipulation/datasource.json');
// FETCHING AND DECODING THE  DATA
$data = json_decode($jsonData, true);

//ITERATE OVER THE DATA AND CHECK WHETHER THE DATA EXIST IN THE DB OR NOT
foreach ($data as $item) {

    //GET THE PARTICIPATIONS DATA
    $id = (int)$item['participation_id'];
    $participation_fee = (float)$item['participation_fee'];
    $employee_name = $conn->real_escape_string($item['employee_name']);
    $employee_mail = $conn->real_escape_string($item['employee_mail']);
    $event_name = $conn->real_escape_string($item['event_name']);
    $event_date = $item['event_date'];
    $event_id = (int)$item['event_id'];


    $existingRecordQuery = "SELECT * FROM participation WHERE id = $id";
    $existingRecordResult = $conn->query($existingRecordQuery);

    if ($existingRecordResult->num_rows == 0) {
        $insertQuery = "INSERT INTO participation (id, event_id, event_name, event_date ,employee_name, employee_mail, participation_fee) VALUES ($id, $event_id, '$event_name', '$event_date', '$employee_name', '$employee_mail', '$participation_fee')";

        if ($conn->query($insertQuery) === false) {
            echo "Error: " . $insertQuery . "<br>" . $conn->error;
        } else {
            echo "Record with ID $id inserted successfully!<br>";
        }
    } else {
        echo "Record with ID $id already exists. Skipping.<br>";
    }
}


$conn->close();

echo "Data import process completed!";
?>