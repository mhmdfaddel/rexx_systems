 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Event Filter</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>

<form method="get" action="index.php">
    <label for="employee_name">Employee Name:</label>
    <input type="text" name="employee_name">

    <label for="event_name">Event Name:</label>
    <input type="text" name="event_name">

    <label for="event_date">Event Date:</label>
    <input type="date" name="event_date">

    <input type="submit" value="Filter">
</form>

<?php
include './data_source_manipulation/EventsDatafetcher.php';
include './data_source_manipulation/ParticipationsDataFetcher.php';
include './data_source_manipulation/db_connection.php';
global$conn;
$employeeName = isset($_GET['employee_name']) ? $_GET['employee_name'] : '';
$eventName = isset($_GET['event_name']) ? $_GET['event_name'] : '';
$eventDate = isset($_GET['event_date']) ? $_GET['event_date'] : '';

$sql = "SELECT * FROM participation ;";

if($employeeName == '' && $eventName == '' && $eventDate == '') {
    $sql = "SELECT * FROM participation ;";
}
else {
    $sql = "SELECT p.employee_name, e.event_name, e.event_date, p.participation_fee
        FROM Participation p
        JOIN Events e ON p.event_id = e.id
        WHERE 
            p.employee_name LIKE '%$employeeName%' AND 
            e.event_name LIKE '%$eventName%' AND 
            e.event_date = '$eventDate'";
}



$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo '<table>';
    echo '<tr><th>Employee Name</th><th>Event Name</th><th>Event Date</th><th>participation_fee</th></tr>';

    $totalparticipation_fee = 0;

    while ($row = $result->fetch_assoc()) {
        $totalparticipation_fee += $row['participation_fee'];
        echo "<tr><td>{$row['employee_name']}</td><td>{$row['event_name']}</td><td>{$row['event_date']}</td><td>{$row['participation_fee']}</td></tr>";
    }

    echo "<tr><td colspan='3'>Total participation_fee</td><td>{$totalparticipation_fee}</td></tr>";

    echo '</table>';
} else {
    echo "No results found.";
}

$conn->close();
?>

</body>
</html>

