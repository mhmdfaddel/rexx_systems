# rexx_systems

a small project for rexx_systems Company

## Getting started

please install the following software to run the app so simply:
- [XAMPP](https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/8.2.12/xampp-windows-x64-8.2.12-0-VS16-installer.exe/download)

## Fetch the Code either

- [ ]  [Gitlab](https://gitlab.com/mhmdfaddel/rexx_systems)
- [ ] [Zip File](https://drive.google.com/file/d/1jeWO6XRj7Ih9sWrjOpeq2ei2_vJ-kjxy/view?usp=sharing) 'Note that the File will be deleted after 10 days starting count from 07.01.2023'

## Host the app within Xampp like the Following:

```
- Extract or fetch the main folder into the htdocs Folder of Xampp.
- Run the Apache and Mysql services.
- Check if the app is running by navigating to any web browser with the Link (localhost/rexx_systems/index.php) 
```

## Database
- [ ] Create a Database with the name 'rexx';
- [ ] Create Two main tables called :
  - event.
  - participation.
    - or you can simply import this [file](https://drive.google.com/file/d/1th3ltYFwF9Yk5S--Z03QBsTGhlO68W7C/view?usp=sharing) into your database.

## Run and Test
- [ ] Since i recommend to use Xampp you don't need to install any further software "Only a project editor and a web browser 😁".
- [ ] Run this [URL](localhost/rexx_systems/index.php) with the Browser and everything shall be okay.

## Side Notes:
- [ ] Since i used to work with Frameworks all my Life i couldn't implement a DTO the method that empowers me to store only a foreign Key and then get the whole object using it, it needs an extra work and i didn't bother myself to do it.
- [ ] You can see that there is an Employee table in the Database scheme, i left it there because i intended to do some Many-to-Many Relationship between the Employee and the Event Tables through the Participation Table.
- [ ] Anyway the App is working right now and since it is written with Pure PHP it should not act like a superhero (from my POV 🤔).  
- [ ] If you reached this far, i want to really thank you from the bottom of my heart you are such a patient Guy or a Women or whatever you identify your self 💙 enjoy the Blue Heart 😉. 


